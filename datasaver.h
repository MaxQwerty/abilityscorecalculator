#ifndef DATASAVER_H
#define DATASAVER_H

#include <QObject>

class DataSaver : public QObject
{
    Q_OBJECT

public slots:
    void saveData(int, int, int, int, int, int, int, QString);
};

#endif // DATASAVER_H

#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "datasaver.h"

int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    DataSaver saver;
    QObject* qmlObj = engine.rootObjects().first();
    QObject::connect(qmlObj, SIGNAL(dataSaved(int, int, int, int, int, int, int, QString)),
                     &saver, SLOT(saveData(int, int, int, int, int, int, int, QString)));

    return app.exec();
}

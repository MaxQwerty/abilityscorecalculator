import QtQuick 2.0
import QtQuick.Controls 2.0

Rectangle
{
    id: abilityControl
    height: childrenRect.height + 10

    property string infoText: ""
    property string name: ""
    property color colorAbility: "black"
    property alias value: spinBox.value

    border.color: colorAbility
    border.width: 2

    property var costsArray: [0,2,1,1,1,1,1,2,2,3,3,4,0]

    function calcAbilityCost(value)
    {
        return costsArray[value-6]
    }

    function calcAbilityBonus(value)
    {
        return Math.floor((value - 10) / 2)
    }

    signal scoreSpended(int score)

    Column
    {
        height: childrenRect.height
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.margins: 5
        Row
        {
            height: childrenRect.height
            width: parent.width
            spacing: 5

            Text
            {
                text: "<font color=\"" + colorAbility + "\">" + abilityControl.name +"</font>"
                width: 50
                anchors.verticalCenter: spinBox.verticalCenter
                font.pointSize: 15
            }
            SpinBox
            {
                id: spinBox
                property int prevValue: 10
                value: 10
                from: 7
                to: 18
                width: 100
                onValueChanged:
                {
                    if(prevValue < value)
                    {
                        scoreSpended(calcAbilityCost(spinBox.value - 1))
                    }
                    else
                    {
                        scoreSpended(-calcAbilityCost(spinBox.value))
                    }

                    prevValue = value
                }
            }
            Rectangle
            {
                height: abilityBonusText.contentHeight + 5
                width: height
                radius: height / 2
                border.color: colorAbility
                anchors.verticalCenter: spinBox.verticalCenter
                Text
                {
                    id: abilityBonusText
                    anchors.fill: parent

                    text: (calcAbilityBonus(spinBox.value) > 0 ? "+" : "") + calcAbilityBonus(spinBox.value)
                    font.pointSize: 12
                    color: colorAbility
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }
            }

            Text
            {
                text: (calcAbilityCost(spinBox.value) != 0
                ? "Добавить: -" + calcAbilityCost(spinBox.value)
                : "")
                +
                ((calcAbilityCost(spinBox.value) != 0) && (calcAbilityCost(spinBox.value - 1) != 0) ? "\n" : "")
                +
                (calcAbilityCost(spinBox.value - 1) != 0
                ? "Вычесть: +" + calcAbilityCost(spinBox.value - 1)
                : "")
                wrapMode: Text.WordWrap
                height: contentHeight
                anchors.verticalCenter: spinBox.verticalCenter
            }
        }
        Text
        {
            text: abilityControl.infoText
            height: contentHeight
            width: parent.width
            wrapMode: Text.WordWrap
        }
    }
}

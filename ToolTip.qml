import QtQuick 2.0

Rectangle
{
    property alias text: textData.text
    width: 100
    height: textData.height + textData.anchors.margins * 2

    color: "white"
    radius: 5

    Text
    {
        id: textData

        wrapMode: Text.WordWrap

        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 5
    }
}

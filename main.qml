import QtQuick 2.7
import QtQuick.Window 2.2

Window {
    id: mainWindow
    visible: true
    width: 300
    height: contentItem.height
    title: "Pathfinder Score Calculator"

    property int totalScore: 15

    signal dataSaved(int Str, int Dex, int Con,
                     int Int, int Wis, int Cha,
                     int Left, string name);

    Item
    {
        id: contentItem
        width: parent.width
        height: childrenRect.height
        clip: true

        ListModel
        {
            id: abilitiesModel
            ListElement {name: "СИЛ"; info: "Физическая сила и мощь. Определяет урон оружия\nПервична для войнов\nВторична для воров"; color: "darkred"}
            ListElement {name: "ЛОВ"; info: "Стремление и координация. Для стрельбы и уворотов.\nПервичная для воров\nВторична для войнов\nВторична для магов"; color: "green"}
            ListElement {name: "ВЫН"; info: "Телосложение и живучесть. Больше здоровья и больше защиты от яда.\nВторична для всех"; color: "red"}
            ListElement {name: "ИНТ"; info: "Смекалка и знания. Даёт больше пунктов навыков(внимание, скрытность, знание подземелий и пр.).\nПервичен для волшебников (16)"; color: "#5511FF"}
            ListElement {name: "МУД"; info: "Сила духа и интуиция. Для более наблюдательных.\nПервична для жрецов (16)"; color: "#8888FF"}
            ListElement {name: "ХАР"; info: "Внушение и привлекательность.\nВторична для жрецов"; color: "orange"}
        }

        Text {
            id: textTotalScore
            text: "Оставшиеся очки: <font color=\"#00AA88\">" + totalScore + "</font>"
            font.pointSize: 12

            width: contentWidth
            height: contentHeight
            anchors.horizontalCenter: parent.horizontalCenter
        }
        Column
        {
            id: controlsContainer
            width: parent.width
            height: childrenRect.height
            anchors.top: textTotalScore.bottom

            spacing: 5

            Repeater
            {
                id: abilitiesContainer
                model: abilitiesModel
                AbilityControl
                {
                    name: model.name
                    infoText: model.info
                    colorAbility: model.color
                    width: parent.width
                    onScoreSpended:
                    {
                        totalScore -= score
                    }
                }
            }
            Item
            {
                id: nameContainer
                height: 20
                width: parent.width

                Text
                {
                    id: labelName
                    height: parent.height
                    width: contentWidth
                    text: "Имя: "
                    verticalAlignment: Text.AlignVCenter
                }

                Rectangle
                {
                    border.color: "black"
                    border.width: 1
                    height: parent.height

                    anchors.left: labelName.right
                    anchors.right: parent.right

                    TextInput
                    {
                        id: nameText
                        anchors.fill: parent
                        anchors.margins: 2
                        verticalAlignment: TextInput.AlignVCenter
                    }
                }
            }
            Rectangle
            {
                width: parent.width
                height: 20
                color: mouseAreaSave.containsMouse && (nameText.text != 0)
                       ? (mouseAreaSave.pressed ? "#444444" : "#888888")
                       : "#aaaaaa"
                Text {
                    text: (nameText.text != 0) ? "Сохранить" : ""
                    height: contentHeight
                    width: contentWidth
                    anchors.centerIn: parent
                    color: mouseAreaSave.pressed ? "white" : "black"
                }
                MouseArea
                {
                    id: mouseAreaSave
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked:
                    {
                        if(nameText.text != 0)
                        {
                            dataSaved(abilitiesContainer.itemAt(0).value,
                                      abilitiesContainer.itemAt(1).value,
                                      abilitiesContainer.itemAt(2).value,
                                      abilitiesContainer.itemAt(3).value,
                                      abilitiesContainer.itemAt(4).value,
                                      abilitiesContainer.itemAt(5).value,
                                      totalScore, nameText.text)
                        }
                    }
                }
            }
        }
    }
}

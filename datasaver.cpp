#include "datasaver.h"

#include <QDebug>
#include <QFile>

void DataSaver::saveData(int str, int dex, int con, int intel, int wis, int cha, int total, QString name)
{
    QFile file(name + QString(".txt"));
    file.open(QFile::WriteOnly);

    QString dataString = QString("СИЛ %1\n"
                                 "ЛОВ %2\n"
                                 "ВЫН %3\n"
                                 "ИНТ %4\n"
                                 "МУД %5\n"
                                 "ХАР %6\n"
                                 "Остаток: %7\n").arg(str).arg(dex).arg(con).arg(intel).arg(wis).arg(cha).arg(total);
    file.write(QByteArray().append(dataString));
    file.close();
}
